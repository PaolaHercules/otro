<!DOCTYPE html>
<html>
<head>
	<title>Hobbies favoritos</title>
    <link rel="stylesheet" type="text/css" href="props/css/bootstrap.css">
    <script type="text/javascript" src="props/js/bootstrap.js"></script>
</head>
<body>

	<style>
        @page {
            margin: 0cm 0cm;
            font-family: Arial;
        }

        body {
            margin: 3cm 2cm 2cm;
        }

        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
            background-color: black;
            color: white;
            text-align: center;
            line-height: 30px;
        }

        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
            background-color: black;
            color: white;
            text-align: center;
            line-height: 35px;
        }
    </style>
    <body>
        <header>
            <h1>Reporte de aficiones CDS3</h1>
        </header>

        <main>

            <table class="table table-dark" border="1">
                <tr>
                    <td>Joven</td>
                    <td>Hobby</td>
                </tr>
                
                <tr>
                @foreach ($show as $aficion)
                    <td>{{ $aficion->nombre }}</td>
                    <td>{{ $aficion->nombre }}</td>
                @endforeach
            </tr>

        </table>

    </main>

    <footer>
        <h1>Elaborado por Paola Hércules</h1>
    </footer>
</body>
</html>