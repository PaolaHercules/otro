@extends('template.header')
<body>

	<br><br>

	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="card">
					<h5 class="card-header">Lista de hobbies</h5>
					<div class="card-body">
						<form action="{{Route('Agregar')}}" method="POST">
							{{ csrf_field() }}
							<select name="id_joven" class="form-control">
								<option>Seleccione un joven</option>

								@foreach($aficiones as $aficion)


								<option value="{{ $aficion->joven_id }}">{{ $aficion->nombre }}</option>
								@endforeach
							</select>
							<br>
							<div class="row">
								<div class="col-md-6">
									<input type="text" name="nombre" class="form-control" placeholder="/* Hobby */">
								</div>

								<div class="col-md-6">
									<input type="number" name="hobby_id" class="form-control" placeholder="/* N° */">
								</div>
							</div>	
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<input type="submit"  style="background-color: #FF8800; border: 0px;" class="btn btn-primary btn-lg btn-block" value="Agregar">
				</form>
				<button type="button" class="btn btn-success btn-lg btn-block">Exportar</button>
				<button type="button" style="background-color: #9933CC; border: 0px;" class="btn btn-primary btn-lg btn-block">Cerrar</button>
			</div>
		</div>
	</div>

