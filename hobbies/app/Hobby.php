<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hobby extends Model
{
	protected $table = 'hobby';
	protected $primaryKey = 'hobby_id';
	protected $fillable = ['hobby_id', 'nombre', 'id_joven'];
	public $timestamps = false;
}
