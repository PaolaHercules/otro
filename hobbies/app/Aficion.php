<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aficion extends Model
{
	protected $table = 'joven';
	protected $primaryKey = 'joven_id';
	protected $fillable = ['joven_id', 'nombre', 'hobby_id'];
	public $timestamps = false;

	public function nombres(){
		return $this->hasMany(Hobby::class,'hobby_id');
	}
}
