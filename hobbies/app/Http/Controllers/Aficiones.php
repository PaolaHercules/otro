<?php

namespace App\Http\Controllers;

use App\Aficion;
use App\Hobby;
use PDF;
use Illuminate\Http\Request;

class Aficiones extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $aficiones = Aficion::all();

        return view('hobbies', compact('aficiones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        Hobby::create($request->all());
        return redirect()->Route('Agregar');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function download()
    {
        $show = Aficion::find(1);
        $pdf = PDF::loadView('jovenHobby', compact('show'));

        return $pdf->stream('hobbies.pdf');
    }


  /*  public function downloadPDF() {
        $show = Aficion::find(1);
        $pdf = PDF::loadView('jovenHobby', compact('show'));
        //Si deseo descargar directamente solo cambio el metodo stream por download
        return $pdf->stream('hobbies.pdf');
    }
*/
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
